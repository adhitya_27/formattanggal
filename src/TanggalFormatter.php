<?php

namespace Adhitya;
class TanggalFormatter
{

  public function format($input)
  {
    $today = date('Y-m-d');
    
    // menentukan timestamp untuk hari berikutnya dari tanggal yang ditentukan
    $nextD = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
    // menentukan timestamp untuk hari sebelumnya dari tanggal yang ditentukan
    $prevD = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));

    if ($input == $today) {
      echo "Hari Ini";
    } elseif ($input == $nextD) {
      echo "Besok";
    } elseif ($input == $prevD) {
      echo "Kemarin";
    } else {
      echo $input;
    }
    
    
  }
}


?>